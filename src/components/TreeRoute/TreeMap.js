import React, { Component } from 'react';
import {
    GoogleMap,
    withScriptjs,
    withGoogleMap,
    Marker,
    InfoWindow,
    DirectionsRenderer,
} from 'react-google-maps';
import { withRouter } from 'react-router-dom'

class TreeMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            treeIsSelected: false,
            positionIsSet: false,
            position: null,
            directions: null,
        }
        this.onMapClick = this.onMapClick.bind(this);
        this.renderDitections = this.renderDitections.bind(this);
    }

    onMapClick(event) {
        this.props.mapCallback(event.latLng);
        this.setState({
            positionIsSet: true,
            position: event.latLng,
        });
    }

    // TODO: THIS IS NOT NEEDED WHEN WE HAVE ENDPOINT
    renderDitections() {

        const directionsService = new window.google.maps.DirectionsService();

        const origin = this.state.position;
        const destination = { lat: this.props.treeMarker.lat, lng: this.props.treeMarker.lng };
    
        directionsService.route(
          {
            origin: origin,
            destination: destination,
            travelMode: window.google.maps.TravelMode.DRIVING
          },
          (result, status) => {
            if (status === window.google.maps.DirectionsStatus.OK) {
              this.setState({
                directions: result
              });
            } else {
              console.error(`error fetching directions ${result}`);
            }
          }
        );
    }

    componentDidMount() {
        this.props.onRef(this);
    }
    componentWillUnmount() {
        this.props.onRef(undefined);
    }

    // END TODO

    render() {
        return (
            <GoogleMap
                defaultZoom={17}
                defaultCenter={{ lat: 20.60807, lng: -103.41469}}
                onClick={this.onMapClick}>   

                <Marker
                    key={0}
                    position={{ lat: this.props.treeMarker.lat, lng: this.props.treeMarker.lng }}
                    onClick={() => this.setState({ treeIsSelected: true })}
                    icon='http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|023F4B'/>

                {   this.state.positionIsSet ? (<>
                    <Marker
                        key={1}
                        position={this.state.position}
                        icon='http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|B8CB53'/>
                </>) : null }

                { this.state.treeIsSelected ? (
                    <InfoWindow 
                        position={{ lat: Number(this.props.treeMarker.lat+0.00002), lng: this.props.treeMarker.lng }}
                        onCloseClick={() => this.setState({ treeIsSelected: false })}>
                        <div
                            className="infowindow-content"
                            onClick={() =>this.props.history.push("/tree/"+this.props.treeMarker.id)}>

                            <div className="infowindow-img">
                                <img src="https://picsum.photos/70" alt="" />
                            </div>
                            <div id={this.props.treeMarker.id} className="infowindow-title">
                                Arbol {this.props.treeMarker.id}
                            </div>
                        </div>
                    </InfoWindow>
                    ) : null }

                { this.state.directions && <DirectionsRenderer directions={this.state.directions} />}
            </GoogleMap>
        );
    }
};

export default withScriptjs(
    withGoogleMap(
        withRouter(TreeMap)
    )
)