import React from 'react';
import { Link } from 'react-router-dom'
import { HOME } from '../../constants/HomePageStrings'
import { DESCRIPTON, START_SEARCH, START_SEARCH_EX, WHAT_IS } from '../../constants/CommonStrings'

import './Home.css'

function Home() {
    return (
        <>
        <div className="container">
            <div className="row ">
                <div className="col-md-12 description-container">
                    <h3>ÁRBOLES ITESO</h3>
                    <div className="home-card">
                        <h4>{ WHAT_IS }</h4>
                        <p>
                            { HOME.HOME_DESC } 
                        </p>
                    </div>
                    <h3 className="search-title">{ START_SEARCH_EX }</h3>
                </div>
            </div>
            <div className="row">
                <div className="col-md-6 search-op-container">
                    <h3>{ HOME.NUMBER_TITLE }</h3>
                    <div className="home-card">
                        <h4>{ DESCRIPTON }</h4>
                        <p>
                           { HOME.NUMBER_DESC } 
                        </p>
                        <Link to="/num-search" className="landpage-btn-link">
                            <button type="button" className="btn search-btn">
                                    { START_SEARCH }
                            </button>
                        </Link>
                    </div>
                </div>
                <div className="col-md-6 search-op-container">
                    <h3>{ HOME.SPECIES_TITLE }</h3>
                    <div className="home-card">
                        <h4>{ DESCRIPTON }</h4>
                        <p>
                           { HOME.SPECIES_DESC } 
                        </p>
                        <Link to="/spe-search" className="landpage-btn-link">
                            <button type="button" className="btn search-btn">
                                { START_SEARCH }
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default Home;
