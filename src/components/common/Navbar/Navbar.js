import React from 'react'
import { Link } from 'react-router-dom'

import { AITESO_TITLE } from '../../../constants/CommonStrings'
import { NAVBAR_ABOUT, NAVBAR_HOME, NAVBAR_LOGIN } from '../../../constants/LandingPageStrings'

import './Navbar.css'

function Navbar() {

  return (
    <>
      <nav className="navbar navbar-expand-md navbar-dark navbar-aiteso">
        <div className="container-fluid">
          <img src={`${process.env.REACT_APP_FILE_ROOT}/images/LogoPAP_ArbolesITESOBlanco.svg`} alt="" className="navitem-logo" />
          <Link className="navbar-brand navitem-title nav-text" to="/home">{ AITESO_TITLE }</Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link to="/home" className="nav-link nav-text">{ NAVBAR_HOME }</Link>
              </li>
              <li className="nav-item">
                <Link to="/about" className="nav-link nav-text">{ NAVBAR_ABOUT }</Link>
              </li>
              <li className="nav-item nav-item-login">
                <Link to="/login" className="nav-link nav-text">{ NAVBAR_LOGIN }</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar;
