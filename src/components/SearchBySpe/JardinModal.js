import React, { Component } from 'react';
import {Modal, Form} from 'react-bootstrap'

import './Sidebar.css'

class JardinModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isChecked: true,
      currentSelection: this.props.selected,
    }
  }

  toggleChange = (e) => {
    let tmp = this.state.currentSelection;
    tmp[e.target.id-1] = !tmp[e.target.id-1];
    this.setState({ currentSelection: tmp });
  }

  onSearch = () => {
    this.props.handle_change(this.state.currentSelection);
  }

  render() {
    return (
      <Modal
        {...this.props}
        size="md"
        animation={false}
        centered
      >
        <Modal.Header>
          <Modal.Title>
            Filtrar por Jardín
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="jardin-modal-body">
          <Form className="modal-form">
            {this.props.content.map((jardin, key) => {
              return (
                <Form.Check
                  key={key}
                  id={jardin.id_jardin+1}
                  label={jardin.nombre}
                  onChange={this.toggleChange}
                  checked={this.state.currentSelection[key]}
                />
              )
            })}
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <button className="btn btn-success" onClick={this.onSearch}>Buscar</button>
        </Modal.Footer>
      </Modal>
    )
  }
}

export default JardinModal
