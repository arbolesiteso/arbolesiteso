import React from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'

import './SpeTile.css'

const SpeTile = props => {
    const { 
        specie = null,
        img_link = ""
    } = props;

    return (
        <div className="spetile-container">
            <img className="spetile-img" src={img_link} alt="" />
            <Link className="spetile-link" to={`/jardin-search/`+specie.id_taxonomia}>
                {specie.nombre}
            </Link>
        </div>
    )
}

SpeTile.propTypes = {
    specie: PropTypes.object.isRequired,
    img_link: PropTypes.string.isRequired
}

export default SpeTile
