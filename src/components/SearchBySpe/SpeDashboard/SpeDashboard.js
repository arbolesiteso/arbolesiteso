import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';

import SpeTile from './SpeTile/SpeTile'

import './SpeDashboard.css'
import { SPEDASHBOARD } from '../../../constants/SpeDashboardStrings'

class SpeDashboard extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentPage: SPEDASHBOARD.INITIAL_PAGE,
			speciesPerPage: SPEDASHBOARD.ITEMS_PER_PAGE
		}
		this.handlePageClick = this.handlePageClick.bind(this);
	}

	handlePageClick({ selected: selectedPage }) {
		this.setState({currentPage: selectedPage});
	}

	componentDidUpdate(prevProps) {
		if(prevProps.speciesList !== this.props.speciesList) {
			this.setState({currentPage: SPEDASHBOARD.INITIAL_PAGE})
		}
	}

	render() {
		const { currentPage, speciesPerPage } = this.state;

		const offset = currentPage * speciesPerPage;
		const currentPageData = this.props.speciesList
			.slice(offset, offset + speciesPerPage)
			.map((specie, index) => {
				return(
					<SpeTile key={index} img_link={"https://picsum.photos/120"} specie={specie} />
				)
			});
		const pageCount = Math.ceil(this.props.speciesList.length / speciesPerPage);

		return (
			<div className="spedashboard-container">
				<div className="spedashboard-title">
					<h4>{ SPEDASHBOARD.TITLE }</h4>
				</div>

				{ currentPageData.length < 1 ? (
					<div className="spedashboard-noselect">
						<h4>{ SPEDASHBOARD.NOT_FOUND }</h4>
					</div>
				) : (
					<div className="spedashboard-grid">
						{currentPageData}
					</div>
				)}
				<ReactPaginate 
					previousLabel={"<<"}
					nextLabel={">>"}
					pageCount={pageCount}
					onPageChange={(this.handlePageClick)}
					pageRangeDisplayed={SPEDASHBOARD.PAGE_RANGE_DISPLAYED}
					marginPagesDisplayed={SPEDASHBOARD.MARGIN_RANGE_DISPLAYED}
					containerClassName={"pagination"}
					previousLinkClassName={"pagination__link"}
					nextLinkClassName={"pagination__link"}
					disabledClassName={"pagination__link--disabled"}
					activeClassName={"pagination__link--active"}
				/>
			</div>
		)
	}
}

SpeDashboard.propTypes = {
	speciesList: PropTypes.array.isRequired
};

export default SpeDashboard
