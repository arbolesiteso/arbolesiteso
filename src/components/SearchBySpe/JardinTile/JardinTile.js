import React from 'react'
import PropTypes from 'prop-types'

import './JardinTile.css'

function JardinTile(props) {
    const { 
        label = ""
    } = props;

    return (
        <div className="jtile-container">
            <button type="button" class="btn">{label}</button>
        </div>
    )
}

JardinTile.propTypes = {
    label: PropTypes.string.isRequired
}

export default JardinTile


