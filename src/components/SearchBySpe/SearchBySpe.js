import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import SpeDashboard from './SpeDashboard/SpeDashboard'

import { SIDEBAR } from '../../constants/SidebarStrings'
import { GET_ALL_SPECIES, GET_TREES_BY_SPECIE, /* GET_ALL_JARDINES */ } from '../../constants/BackEndRoutes'

import './Sidebar.css'
import './SearchBySpe.css';
/* import JardinModal from './JardinModal'; */

class SearchBySpe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allSpecies: null,
            speciesList: null,
            allJardines: null,
            jardinesSelected: [],
            letterSelected: "",
            modalShow: false,
            isLoaded: false,
            errorLoading: null
        }
        this.filterSpeciesBySelection = this.filterSpeciesBySelection.bind(this);
        this.handleJardinModalCallback = this.handleJardinModalCallback.bind(this);
        this.processAllSpecies = this.processAllSpecies.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    filterSpeciesBySelection(selection) {
        const newList = this.state.allSpecies.filter( (specie) => {
            /* let cnt = 0;
            for(const jardinId of specie.jardines){
                if(this.state.jardinesSelected[jardinId] === false)
                    cnt += 1;
            }
            if(specie.jardines.length === cnt)
                return false; */

            let currentSpeFirstChar = String(specie.nombre).charAt(0);
            if (selection.localeCompare(currentSpeFirstChar) === 0){
                return true;
            }
            else return false;
        });
        this.setState({ speciesList: newList, letterSelected: selection });
    }

    handleClose(){
        this.setState({ modalShow: false })
    }

    handleJardinModalCallback(jardinesSelected) {
        const newList = this.state.allSpecies.filter( (specie) => {
            let cnt = 0;
            for(const jardinId of specie.jardines){
                if(this.state.jardinesSelected[jardinId] === false)
                    cnt += 1;
            }
            if(specie.jardines.length === cnt)
                return false;

            let currentSpeFirstChar = String(specie.nombre).charAt(0);
            if (this.state.letterSelected === "" || this.state.letterSelected.localeCompare(currentSpeFirstChar) === 0){
                return true;
            }
            else return false;
        });
        this.setState({
            jardinesSelected,
            modalShow: false,
            speciesList: newList
        })
    }

    async processAllSpecies(allSpecies) {
        let speciesList = [];
        for(const specie of allSpecies){
            try {
                let getTreesByTaxIdResponse = await fetch(GET_TREES_BY_SPECIE + specie.id_taxonomia)
                let treesByTaxId = await getTreesByTaxIdResponse.json();
                let jardinListBySpecie = [];
                for(let j = 0; j < treesByTaxId.length; j++) {
                    if(!jardinListBySpecie.includes(treesByTaxId[j].id_jardin))
                        jardinListBySpecie.push(treesByTaxId[j].id_jardin)
                }
                let newSpecieElement = {
                    nombre: specie.nombre, 
                    id_taxonomia: specie.id_taxonomia,
                    jardines: jardinListBySpecie,
                }
                speciesList.push(newSpecieElement);
            } catch(error) {
                console.error(error)
            }
        }
        return speciesList;
    }

    componentDidMount() {
        let getAllSpeciesResponse = fetch(GET_ALL_SPECIES).then(res => res.json());
        getAllSpeciesResponse.then(
            (allSpecies) => {
                allSpecies.sort((a,b) => {
                    var nameA=a.nombre.toLowerCase(), nameB=b.nombre.toLowerCase();
                    if (nameA < nameB)
                        return -1;
                    if (nameA > nameB)
                        return 1;
                    return 0;
                });
                this.setState({
                    isLoaded: true,
                    allSpecies: allSpecies,
                    speciesList: allSpecies,
                });
                /* this.processAllSpecies(allSpecies).then((speciesProcessed) => {
                    let getAllJardinesResponse = fetch(GET_ALL_JARDINES).then(res => res.json());
                    getAllJardinesResponse.then(
                        (allJardines) => {
                            speciesProcessed.sort((a,b) => {
                                var nameA=a.nombre.toLowerCase(), nameB=b.nombre.toLowerCase();
                                if (nameA < nameB)
                                    return -1;
                                if (nameA > nameB)
                                    return 1;
                                return 0;
                            });
                            this.setState({
                                isLoaded: true,
                                allSpecies: speciesProcessed,
                                speciesList: speciesProcessed,
                                allJardines,
                                jardinesSelected: [...Array(allJardines.length)].map((val, i) => true)
                            });
                        },
                        (err) => {
                            this.setState({
                                isLoaded: true,
                                errorLoading: err
                            });
                        }
                    );
                }); */
            },
            (err) => {
                this.setState({
                    isLoaded: true,
                    errorLoading: err
                });
            }
        );
    }

    render() {
        if (this.state.errorLoading) {
            return <div>Error: {this.state.errorLoading.message}</div>;
        } else if (!this.state.isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="spe-container">
                    <div className="sidebar">
                        <div className="sidebar-content btn-group" role="group">
                            { SIDEBAR.CONTENT.map((value, key) => {
                                return (
                                    <div key={key} className="dropdown">
                                    <button className="btn sidebar-btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        { value.label }
                                    </button>
                                    <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        { value.content.map((inside_val, inside_key) => {
                                            return  (
                                                <li key={inside_key}>
                                                    <button 
                                                        className="dropdown-item sidebar-sub-item"
                                                        onClick={() => this.filterSpeciesBySelection(inside_val)}
                                                    >
                                                        {inside_val}
                                                    </button>
                                                </li>
                                            )
                                        })}
                                    </ul>
                                </div>
                                )
                            })}
                             <div key="todo" className="sidebar-jardin-row">
                                <Link to="/jardin-search/all" className="">
                                    <button 
                                     type="button" 
                                     className="btn sidebar-btn filt-jardin-btn"
                                    >
                                        BUSCAR TODO
                                    </button>
                                </Link>
                            </div> 
                            <div key="jardin" className="sidebar-jardin-row">
                                <Link to="/spe-search" className="">
                                    <button 
                                     type="button" 
                                     className="btn sidebar-btn filt-jardin-btn" 
                                     onClick={() => this.setState({ modalShow: true })}
                                     hidden
                                    >
                                        { SIDEBAR.FILT_JARDIN }
                                    </button>
                                </Link>
                            </div>
        
                            {/* <JardinModal 
                                show={this.state.modalShow} 
                                onHide={() => this.setState({ modalShow: false })} 
                                content={this.state.allJardines} 
                                selected={this.state.jardinesSelected} 
                                handle_change={this.handleJardinModalCallback}
                            /> */}
                        </div>
                    </div>
                    <SpeDashboard speciesList={this.state.speciesList}/>
                </div>
            );
        }
    }
}

export default SearchBySpe;
