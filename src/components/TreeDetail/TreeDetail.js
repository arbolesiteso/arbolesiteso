import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { TREE_DETAIL } from "../../constants/TreeDetailStrings";
import { GET_SINGLE_ARBOL, GET_TAXONOMIA_BY_FOLIO } from '../../constants/BackEndRoutes';

import './TreeDetail.css'

function TreeDetail({ match }) {
    useEffect(() => {
        fetchTree();
    }, []);

    const [tree, setTree] = useState({});
    const [isValidId, setIsValidId] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [tax, setTax] = useState({});

    const fetchTree = async () => {
        const fetchSingleArbol = fetch(GET_SINGLE_ARBOL +  match.params.id).then(res => res.json());
        fetchSingleArbol.then((res) => {
          if (res.length > 0) {
            setTree(res[0]);
            setIsValidId(true);
            const fetchTaxonomy = fetch(GET_TAXONOMIA_BY_FOLIO + res[0].id_taxonomia).then(res => res.json());
            fetchTaxonomy.then((taxonomy) => {
              setTax(taxonomy[0]);
              setIsLoading(false);
            }, (err) => {
              console.log(err);
              setIsLoading(false);
            });
          } else {
            setIsValidId(false);
          }
          
        }, (err) => {
          console.log(err);
          setIsLoading(false);
          setIsValidId(false);
        });
    };

    if (isLoading) {
      return ( <> Loading... </>)
    } else if (isValidId) {

      const fitoIcon = (tree.Valoracion < 25) ? 'val_bad.png' : 
        (tree.Valoracion >= 25 && tree.Valoracion < 50) ? 'val_regular.png' :
        (tree.Valoracion >= 50 && tree.Valoracion < 75) ? 'val_good.png' :
        'val_excelent.png';

      const diamIcon = (tree.Diametro >= 30) ? 'diam_big.png' :
            (tree.Diametro < 30 && tree.Diametro >= 15) ? 'diam_med.png':
            'diam_small.png';

      const sizeIcon = (tree.Altura < 8) ? 'height_short.png' : 
            (tree.Altura >= 8 && tree.Altura < 15) ? 'height_med.png' :
            'height_tall.png';

      const humIcon = (tree.humidity >= 70) ? 'hum_high.png' : 
          (tree.humidity < 70 && tree.humidity >= 22) ? 'hum_good.png' :
          'hum_low.png';

      const temIcon = (tree.temperature < 17) ? 'tem_cold.png' : 
          (tree.temperature >= 17 && tree.temperature < 27) ? 'tem_good.png' :
          'tem_hot.png';

      return (
        <>
          <div className="tree-detail-container">
              <div className="row">
                <div className="col-md-12">
                    <h4 className="tree-detail-title">
                      { `${TREE_DETAIL.TITLE}${tree.NID}`}
                    </h4>
                </div>
              </div>
              <div className="row">
                <div className="col-md-8 img-container-wrapper">
                  <div className="img-container card">

                  </div>
                  <p>
                    { TREE_DETAIL.ADDITIONAL_TEXT }
                  </p>
                </div>
                <div className="col-md-4 detail-container-wrappper">
                  <div className="detail-container card">
                    <p>{ TREE_DETAIL.FITO }</p>
                    <p id="fito">
                      <img className="tree-icon" src={`${process.env.REACT_APP_FILE_ROOT}/images/treeIcons/${fitoIcon}`} />
                      { tree.Valoracion }%
                    </p>
                    <p>{ TREE_DETAIL.TAXONOMY }</p>
                    <p id="taxo">{ tax.nombre }</p>
                    <p>{ TREE_DETAIL.DATE }</p>
                    <p id="date">{ tree.Plantado }</p>
                    <p>{ TREE_DETAIL.DIAMETER }</p>
                    <p id="diameter">
                      <img className="tree-icon" src={`${process.env.REACT_APP_FILE_ROOT}/images/treeIcons/${diamIcon}`} />
                      { tree.Diametro }cm
                    </p>
                    <p>{ TREE_DETAIL.SIZE }</p>
                    <p id="size">
                      <img className="tree-icon" src={`${process.env.REACT_APP_FILE_ROOT}/images/treeIcons/${sizeIcon}`} />
                      { tree.Altura }m
                    </p>
                    { tree.humidity != null ? ( <>
                      <p>{ TREE_DETAIL.HUMIDITY }</p>
                      <p id="humidity">
                        <img className="tree-icon" src={`${process.env.REACT_APP_FILE_ROOT}/images/treeIcons/${humIcon}`} />
                        { tree.humidity }%
                      </p>
                    </>) : '' }
                    { tree.temperature != null ? ( <>
                      <p>{ TREE_DETAIL.TEMPERATURE }</p>
                      <p id="temperature">
                      <img className="tree-icon" src={`${process.env.REACT_APP_FILE_ROOT}/images/treeIcons/${temIcon}`} />
                        { tree.temperature }°C
                      </p>
                    </>) : ''}
                  </div>
                  <Link to={`/tree/${tree.NID}/route`} className="landpage-btn-link">
                    <button type="button" className="btn search-btn">
                        { TREE_DETAIL.ROUTE }
                    </button>
                  </Link>
                </div>
              </div>
          </div> 
        </>
      )
    } else {
      return ( <> 
        <div className="tree-detail-container">
          <div className="row">
              <div className="col-md-12">
                  <h4 className="tree-detail-title">
                    { TREE_DETAIL.NOT_FOUND }
                  </h4>
              </div>
            </div>
        </div>
      </> )
    }



}

export default TreeDetail;
