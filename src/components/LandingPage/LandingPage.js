import React from 'react'
import { Link } from 'react-router-dom'

import './LandingPage.css'

function LandingPage() {
  return (
    <>
      <div className="landpage-container">
        <Link to="/home">
          <img src={`${process.env.REACT_APP_FILE_ROOT}/images/LogoPAP_ArbolesITESO.svg`} className="landpage-logo" />
        </Link>
      </div>
    </>
  )
}

export default LandingPage
