import React, { Component } from 'react'

import './FiltroJardin.css'
import { FILT_JARDIN } from './FiltroJardinStrings'
import { BTN_COLORS } from './markers'
import { GET_TREES_BY_SPECIE, GET_JARDIN_BY_ID, GET_TAXONOMIA_BY_FOLIO, GET_ALL_TREES } from '../../constants/BackEndRoutes'
import Map from './Map'

export class FiltroJardin extends Component {
    constructor(props) {
        super(props);
        this.specie_id = props.match.params.specie_id;
        this.state = {
            specie_name: "",
            allTreesBySpecieId: null,
            treesFiltered: null,
            jardinesSelected: null,
            jardinListBySpecie: null,
        }
        this.filterTreesBySelection = this.filterTreesBySelection.bind(this);
    }

    filterTreesBySelection() {
        const { allTreesBySpecieId, jardinesSelected, jardinListBySpecie } = this.state;

        const newList = allTreesBySpecieId.filter( (tree) => {
            for(let i=0; i < jardinesSelected.length; i++){
                if(!jardinesSelected[i]) {
                    if(jardinListBySpecie[i].id_jardin === tree.id_jardin) {
                        console.log(tree);
                        return false;
                    }
                }
            }
            return true;
        });
        this.setState({ treesFiltered: newList })
    }

    handleClick = (e) => {
        let tmp = this.state.jardinesSelected;
        tmp[e.target.id] = !tmp[e.target.id];
        this.setState({
            jardinesSelected: tmp
        })
        console.log(this.state.jardinesSelected)
        this.filterTreesBySelection();
        console.log(this.state.treesFiltered.length)
    }

    async processJardines(jardinIdList) {
        let jardinListBySpecie = [];
        for(const jardinId of jardinIdList){
            try {
                let jardinResponse = await fetch(GET_JARDIN_BY_ID + jardinId)
                let jardin = await jardinResponse.json();
                jardinListBySpecie.push(jardin[0]);
            } catch(error) {
                console.error(error)
            }
        }
        jardinListBySpecie.sort((a,b) => {
            var nameA=a.id_jardin, nameB=b.id_jardin;
            if (nameA < nameB)
                return -1;
            if (nameA > nameB)
                return 1;
            return 0;
        });
        return jardinListBySpecie;
    }

    componentDidMount() {
        if(this.specie_id === "all") {
            console.log("here")
            let getAllTreesResponse = fetch(GET_ALL_TREES).then(res => res.json());
            getAllTreesResponse.then(
                (allTrees) => {
                    let jardinIdList = [];
                    for(const tree of allTrees) {
                        if(!jardinIdList.includes(tree.id_jardin))
                        jardinIdList.push(tree.id_jardin);
                    }
                    this.processJardines(jardinIdList).then(
                        (jardinListBySpecie) => {
                            this.setState({
                                allTreesBySpecieId: allTrees,
                                treesFiltered: allTrees,
                                jardinListBySpecie,
                                jardinesSelected: [...Array(jardinListBySpecie.length)].map((val, i) => true),
                                isLoaded: true
                            })
                        }
                    );
                },
                (err) => {
                    this.setState({
                        isLoaded: true,
                        errorLoading: err
                    });
                }
            )
        } else {
            console.log("here")
            let getSpecieByIdResponse = fetch(GET_TAXONOMIA_BY_FOLIO + 1).then(res => res.json());
            getSpecieByIdResponse.then( 
                (specie) => {
                    this.setState({ specie_name: specie[0].nombre });
                },
                (err) => {
                    this.setState({
                        isLoaded: true,
                        errorLoading: err
                    });
                }
            )
            let getAllTreesBySpecieIdResponse = fetch(GET_TREES_BY_SPECIE + 1).then(res => res.json());
            getAllTreesBySpecieIdResponse.then(
                (allTreesBySpecieId) => {
                    let jardinIdList = [];
                    for(const tree of allTreesBySpecieId) {
                        if(!jardinIdList.includes(tree.id_jardin))
                        jardinIdList.push(tree.id_jardin);
                    }
                    this.processJardines(jardinIdList).then(
                        (jardinListBySpecie) => {
                            this.setState({
                                allTreesBySpecieId,
                                treesFiltered: allTreesBySpecieId,
                                jardinListBySpecie,
                                jardinesSelected: [...Array(jardinListBySpecie.length)].map((val, i) => true),
                                isLoaded: true
                            })
                        }
                    );
                },
                (err) => {
                    this.setState({
                        isLoaded: true,
                        errorLoading: err
                    });
                }
            )
        }
    }

    render() {
        if (this.state.errorLoading) {
            return <div>Error: {this.state.errorLoading.message}</div>;
        } else if (!this.state.isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="filtjar-container">
                    <div className="filtjar-title">
                        <h4>{ FILT_JARDIN.TITLE }</h4>
                    </div>
                    <div className="filtjar-subtitle">
                        <h6>Especie: { this.state.specie_name }</h6>
                    </div>
                    <div className="filtjar-dashboard">
                        <div className="filtjar-sidebar">
                            <div className="filtjar-sidebar-content">
                                { this.state.jardinListBySpecie.map((jardin, key) => {
                                    return (
                                        <>
                                            <button 
                                                key={key}
                                                id={key}
                                                className={this.state.jardinesSelected[key] ? `btn filtjar-btn color_${BTN_COLORS[jardin.id_jardin]}` : "btn filtjar-btn isNotSelected"}
                                                onClick={this.handleClick}
                                            >
                                                {jardin.nombre}
                                            </button>
                                        </>
                                    )
                                })}
                            </div>
                        </div>
                        <div className="filtjar-mapa">
                            <Map 
                                googleMapURL={`https://maps.googleapis.com/maps/api/js?v3.exp?libraries=drawing&key=${process.env.REACT_APP_GOOGLE_API_KEY}`}
                                containerElement={<div style={{height:'600px', width:'100%'}} />}
                                mapElement={<div style={{ height:'100%', width:'100%'}} />}
                                loadingElement={<p>Cargando Vista...</p>}
                                markers={this.state.treesFiltered}
                            />
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default FiltroJardin

