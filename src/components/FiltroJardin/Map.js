import React, { Component } from 'react';
import {
    GoogleMap,
    withScriptjs,
    withGoogleMap,
    Marker,
    InfoWindow
} from 'react-google-maps'
import Polygon from 'react-google-maps/lib/components/Polygon';
import { withRouter } from 'react-router-dom'
import { MARKERS_ICON } from './markers'
import { JARDIN_COORDS } from './jardinCoords'

class Map extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedMarker: null,
        }
    }

    render() {
        return (
            <GoogleMap
                defaultZoom={17}
                defaultCenter={{ lat: 20.60807, lng: -103.41469}}
            >   
                {this.props.markers.map( (marker, index) => {
                    return (
                        <Marker
                            key={index}
                            position={{ lat: Number(marker.Latitud), lng: Number(marker.Longitud) }}
                            onClick={() => this.setState({ selectedMarker:marker })}
                            icon={MARKERS_ICON[marker.id_jardin].icon}
                        />
                    )}
                )}
                { this.state.selectedMarker ? (
                    <InfoWindow 
                        position={{ lat: Number(this.state.selectedMarker.Latitud)+0.00002, lng: Number(this.state.selectedMarker.Longitud) }}
                        onCloseClick={() => this.setState({ selectedMarker: null })}
                    >
                        <div
                            className="infowindow-content"
                            onClick={() =>this.props.history.push("/tree/"+this.state.selectedMarker.NID)}
                        >
                            <div className="infowindow-img">
                                <img src="https://picsum.photos/70" alt="" />
                            </div>
                            <div 
                                id={this.state.selectedMarker.id}
                                className="infowindow-title"
                            >
                                Arbol {this.state.selectedMarker.NID}
                            </div>
                        </div>
                    </InfoWindow>
                    ) : null
                }
                {
                    JARDIN_COORDS.map((jardin, key) => {
                        return (
                            <Polygon
                                path={jardin.coords}
                                options={
                                    {
                                        fillColor: jardin.fillColor,
                                        strokeColor: jardin.strokeColor,
                                        fillOpacity: jardin.fillOpacity,
                                    }
                                }
                            />
                        )
                    })
                }
            </GoogleMap>
        );
    }
};

export default withScriptjs(
    withGoogleMap(
        withRouter(Map)
    )
)