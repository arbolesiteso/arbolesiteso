const MARKERS_ICON = [
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|043F4B' },
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|3C5D26' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|006A7D' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|EFA74A' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|B9CC53' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|CE934F' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|C2D8ED' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|8D9449' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|B282B8' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|12AECA' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|D678AE' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|63AA33' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|5D66AD' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|BA5269' }, 
    { icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|3B406D' }, 
]

const BTN_COLORS = [
    "043F4B", "3C5D26", "006A7D", "EFA74A", "B9CC53",
    "CE934F", "C2D8ED", "8D9449", "B282B8", "12AECA",
    "D678AE", "63AA33", "5D66AD", "BA5269", "3B406D"
]

module.exports = {
    MARKERS_ICON,
    BTN_COLORS
}