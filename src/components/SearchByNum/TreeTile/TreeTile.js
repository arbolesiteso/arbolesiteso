import React from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'

import './TreeTile.css'

const TreeTile = props => {
    const { 
        id = -1,
        specie = "",
        color = "",
        imgSrc = "",
    } = props;

    return (
        <Link to={`/tree/${id}`}>
          <div className="tree-result-container">
              <div className="tree-result" style={{backgroundColor: color}}>
                  <img src={imgSrc}/>
                  <p>{ id }</p>
                  <p>{ specie }</p>
              </div>
          </div>
        </Link>
    )
}

TreeTile.propTypes = {
    id: PropTypes.number.isRequired,
    specie: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    imgSrc: PropTypes.string.isRequired,
}

export default TreeTile
