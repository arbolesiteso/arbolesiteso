import React, { Component } from 'react';

import { NUM_SEARCH } from '../../constants/NumSearchStrings'
import { INVENTARIO_LAST_FOLIO_CREATED, GET_SINGLE_ARBOL, GET_TAXONOMIA_BY_FOLIO, RANDOM_INVENTARIOS } from '../../constants/BackEndRoutes'
import { RECOMMENDED_TREES , RESULT_TREE } from "../../constants/trees_test";
import TreeTile from './TreeTile/TreeTile';

import './SearchByNum.css'

class SearchByNum extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            searchResult: {},
            searchRecomended: [],
            totalTrees: null,
            isLoaded: false,
            errorLoading: null,
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }
    async handleSearch(event) {
        event.preventDefault();
        if (this.state.inputValue != '') {
            this.setState({
                isLoaded: false,
            });

            const fetchSingleArbol = fetch(GET_SINGLE_ARBOL + this.state.inputValue).then(res => res.json());
            fetchSingleArbol.then((singleArbol) => {
                this.processTree(singleArbol, true);
            }, (err) => {
                this.setState({
                    isLoaded: true,
                    errorLoading: err
                });
            })

            const fetchRecomendedArbol = fetch(RANDOM_INVENTARIOS).then(res => res.json());
            fetchRecomendedArbol.then((recommended) => {
                this.processTree(recommended, false);
            }, (err) => {
                this.setState({
                    isLoaded: true,
                    errorLoading: err
                });
            })

        } else {
            this.setState({
                searchResult: {},
                searchRecomended: [],
            });
        }
    }
    async processTree(treeRes, isSearchResult) {
        await Promise.all(treeRes.map( async tree => {
            const fetchTaxonomy = await fetch(GET_TAXONOMIA_BY_FOLIO + tree.id_taxonomia);
            const taxonomy = await fetchTaxonomy.json();
            tree.id = tree.NID;
            tree.specie = taxonomy[0].nombre;
        }))
        if (isSearchResult) {
            this.setState({
                isLoaded: true,
                searchResult: treeRes[0],
            })
        } else {
            this.setState({
                isLoaded: true,
                searchRecomended: treeRes,
            })
        }
    }
    handleInputChange(event) {        
        this.setState({
            inputValue: event.target.value
        });
    }
    componentDidMount() {
        let fetchTotalTrees = fetch(INVENTARIO_LAST_FOLIO_CREATED).then(res => res.json());
        fetchTotalTrees.then( totalTreesRes => {
            this.setState({
                totalTrees: Number(totalTreesRes[0].NID),
                isLoaded: true
            });
        }, (err) => {
            this.setState({
                isLoaded: true,
                errorLoading: err
            });
        })
    }
    render() {
        if (this.state.errorLoading) {
            return <div>Error: {this.state.errorLoading.message}</div>;
        } else if (!this.state.isLoaded) {
            return <div>Loading...</div>;
        } else {
            const isSearchValid = this.state.searchRecomended.length > 0;
            const colors = ["#C1D8ED", "#B8CB53", "#00697C"];
    
            const resultRender = (isSearchValid) ? <TreeTile 
                                    id={ this.state.searchResult.id }
                                    specie={ this.state.searchResult.specie }
                                    color="#EEA649"
                                    imgSrc="https://picsum.photos/120"/> : '';
                                    
            const recommendedRender = this.state.searchRecomended.map((tree, index) => {
                return(
                    <TreeTile 
                        key={ index }
                        id={ tree.id }
                        specie={ tree.specie }
                        color={ colors[index] } 
                        imgSrc="https://picsum.photos/120"/>
                )
            });

            return (
                <>
                <div className="num-container">
                    <div className="row">
                        <div className="col-md-12">
                            <h4 className="num-title">
                                { NUM_SEARCH.NUMBER_TITLE }
                            </h4>
                        </div>
                    </div>
                    <div className="search-container">
                        <div className="row">
                            <div className="col-md-8">
                                <form onSubmit={this.handleSearch} className="num-search-field">
                                    <input
                                        id="searchhByNum"
                                        type="number"
                                        min="0" max={this.state.totalTrees}
                                        value={this.state.inputValue}
                                        onChange={this.handleInputChange}
                                        placeholder="ingresar número"/>
                                    <input type="submit" value="Submit"/>
                                    { !isSearchValid ? (<>
                                        <p>
                                            {`${NUM_SEARCH.SEARCH_NUMBER}${this.state.totalTrees}`}
                                        </p>
                                    </>) : ''}

                                </form>
                            </div>
                        </div>
                        { isSearchValid ? ( <>
                            <div className="row tree-container">
                                <div className="col-md-12 tree-result-wrapper">
                                    { resultRender }
                                </div>
                            </div>
                            <div className="row tree-container">
                                <p>
                                    { NUM_SEARCH.RECOMMENDED }
                                </p>
                                <div className="col-md-12 tree-result-wrapper">
                                    { recommendedRender }
                                </div>
                            </div> 
                        </>) : '' }
                    </div>
                </div>
                </>
            )
        }
    }
}

export default SearchByNum;
