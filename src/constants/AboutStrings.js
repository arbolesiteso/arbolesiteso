const ABOUT = {
  ARBOLES: 'ÁRBOLES',
  ITESO: 'ITESO',
  MISION: 'JUSTIFICACIÓN',
  VISION: 'ANTECEDENTES',
  ESTUDIANTES: 'ESTUDIANTES',
  WHAT_TEXT: 'Es una aplicación web capaz de otorgar datos al usuario sobre un árbol específico, en lo que es conocido como bosque ITESO. Esta información es otorgada gracias al censo "forestal" o dasonómico realizado por el departamente de Servicioes Generales ITESO.',
  JUST_TEXT: 'Esta aplicación fue desarrollada con la finalidad de: <ul><li>Preservar la diversidad forestal del ITESO, buscando mantener los 40 metros cuadrados de zona verde por habitante que tiene el ITESO en sus instalaciones.</li><li>Informara los usuarios y comunidad universitaria sobre las especies de árboles existentes en sus áreas verdes.</li><ul>',
  ANT_TEXT: 'En 2014,  se realizó una primera versión de ‘Árboles ITESO’ en el cual se tenía la capacidad de hacer consultas de los árboles registrados en el ITESO, utilizando como entrada de búsqueda ya sea su número identificador o la especie que pertenezca. Esta aplicación daba al usuario información general del estado del árbol, su aspecto físico y varios valores calculados por medio de sensores. De igual forma, se tenía la capacidad de dar al usuario una forma de localizarse y después mostrar la ruta más corta a seguir para llegar al árbol seleccionado.<br> Al término de este proyecto, se tuvo la plataforma funcionando en web y en móvil, sin embargo, no todos los árboles tenían sensores por lo que la información que estos mandaban era limitada a un cierta cantidad de árboles. De igual forma, no se hizo continuidad a este proyecto por lo que demás actualizaciones y progreso quedó pausado y en la actualidad, mucho de lo que se hizo ya quedó obsoleto y existen nuevas herramientas que beneficiarían al desarrollo de esta aplicación.<br>Es por esto que se decidió hacer una nueva aplicación que utilice lo que se planteó cuando se creó la aplicación en 2014 pero aplicando nuevas técnicas de trabajo y tecnologías actuales. ',
  STUDENTS: '<h4>Semestre Primavera 2021</h4>'+
  '<h5>Diseño</h5><p>Lic. en Diseño - Nathalie Zúñiga Saldaña</p>'+
  '<h5>Backend</h5><p>Lic. En Ing. En Sistemas Computacionales – Erick Cardona Soto Maynez<br>Lic. En Ing. En Sistemas Computacionales – Omar Pérez Cano</p>'+
  '<h5>Frontend</h5><p>Lic. En Ing. En Sistemas Computacionales – Luis Ricardo Díaz Flores<br>Lic. En Ing. En Sistemas Computacionales – Marcos Antonio Fierros Estrada</p>'
}

module.exports = {
  ABOUT
}
