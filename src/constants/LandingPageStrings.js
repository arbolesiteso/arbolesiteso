const NAVBAR_ABOUT = "About";
const NAVBAR_HOME = "Home";
const NAVBAR_LOGIN = "Log In";
const LANDPAGE_DESCRIPTION = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like.";
const LANDPAGE_HOME_BTN_TEXT = "Comienza tu Búsqueda";

module.exports = {
    NAVBAR_ABOUT,
    NAVBAR_HOME,
    NAVBAR_LOGIN,
    LANDPAGE_DESCRIPTION,
    LANDPAGE_HOME_BTN_TEXT
}