const TREE_DETAIL = {
  TITLE: "ÁRBOL NÚMERO ",
  NOT_FOUND: "Árbol no encontrado",
  ADDITIONAL_TEXT: "Árbol (texto adicional)",
  FITO: "Fitopatología",
  TAXONOMY: " Taxonomía",
  DATE: "Año Plantado",
  DIAMETER: "Diámetro",
  SIZE: "Porte",
  HUMIDITY: "Humedad",
  TEMPERATURE: "Temperatura",
  ROUTE: "Ruta"
}

module.exports = {
  TREE_DETAIL
}
