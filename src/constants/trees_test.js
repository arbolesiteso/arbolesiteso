const RECOMMENDED_TREES = [
  { id: 1, specie: "Durazno"},
  { id: 2, specie: "Ébano"},
  { id: 3, specie: "Ficus"},
]

const RESULT_TREE = {
  id: 4, specie: "Ficus"
}

const ALL_TREES = [
  { id: 1, specie: "Durazno", fito: 50, date: "2010", diameter: 30, size: 4, humidity: 80, temperature: 10},
  { id: 2, specie: "Ébano", fito: 80, date: "1997", diameter: 10, size: 13, humidity: 75, temperature: 18},
  { id: 3, specie: "Ficus", fito: 100, date: "2020", diameter: 15, size: 10, humidity: 50, temperature: 20},
  { id: 4, specie: "Ficus", fito: 5, date: "1995", diameter: 50, size: 20, humidity: 5, temperature: 30},
]

module.exports = {
  RECOMMENDED_TREES,
  RESULT_TREE,
  ALL_TREES
}