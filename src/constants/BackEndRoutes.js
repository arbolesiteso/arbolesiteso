export const GET_ALL_SPECIES = "http://papvidadigital-test.com/P2021API/api/arboles/tiposArboles";
export const GET_TREES_BY_SPECIE = "http://papvidadigital-test.com/P2021API/api/arboles/InventarioFilter/taxonomia/";
export const GET_ALL_JARDINES = "http://papvidadigital-test.com/P2021API/api/arboles/jardines";
export const GET_JARDIN_BY_ID = "http://papvidadigital-test.com/P2021API/api/arboles/jardines/"
export const GET_ALL_TREES = "http://papvidadigital-test.com/P2021API/api/arboles/inventario";

export const GET_SINGLE_ARBOL = "http://papvidadigital-test.com/P2021API/api/arboles/inventario/";
export const INVENTARIO_LAST_FOLIO_CREATED = "http://papvidadigital-test.com/P2021API/api/arboles/lastCreated";
export const RANDOM_INVENTARIOS = "http://papvidadigital-test.com/P2021API/api/arboles/InventarioSearch";
export const GET_TAXONOMIA_BY_FOLIO = "http://papvidadigital-test.com/P2021API/api/arboles/tiposArboles/"