const NUM_SEARCH = {
  NUMBER_TITLE : "BÚSQUEDA POR NÚMERO",
  SEARCH_NUMBER : "Búsqueda desde 0 hasta ",
  RECOMMENDED: "También te podría interesar. .",
}

module.exports = {
  NUM_SEARCH,
}