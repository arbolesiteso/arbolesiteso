const SIDEBAR = {
    CONTENT: [
        {
            label: "ESPECIES DE A - D",
            link: "/spe-search",
            content: ['A', 'B', 'C', 'D']
        },
        {
            label: "ESPECIES DE E - H",
            link: "/spe-search",
            content: ['E', 'F', 'G', 'H']
        },
        {
            label: "ESPECIES DE I - L",
            link: "/spe-search",
            content: ['I', 'J', 'K', 'L']
        },
        {
            label: "ESPECIES DE M - O",
            link: "/spe-search",
            content: ['M', 'N', 'Ñ', 'O']
        },
        {
            label: "ESPECIES DE P - S",
            link: "/spe-search",
            content: ['P', 'Q', 'R', 'S']
        },
        {
            label: "ESPECIES DE T - W",
            link: "/spe-search",
            content: ['T', 'U', 'V', 'W']
        },
        {
            label: "ESPECIES DE X - Z",
            link: "/home",
            content: ['X', 'Y', 'Z']
        }
    ],
    FILT_JARDIN: "FILTRADO POR JARDIN"
}

module.exports = {
    SIDEBAR
}
