const JARDINES = [
    {id: 1, label: "Jardín 1"},
    {id: 2, label: "Jardín 2"},
    {id: 3, label: "Jardín 3"},
    {id: 4, label: "Jardín 4"},
    {id: 5, label: "Jardín 5"},
    {id: 6, label: "Jardín 6"},
    {id: 7, label: "Jardín 7"},
    {id: 8, label: "Jardín 8"},
    {id: 9, label: "Jardín 9"},
    {id: 10, label: "Jardín 10"},
    {id: 11, label: "Jardín 11"},
    {id: 12, label: "Jardín 12"},
]

module.exports = {
    JARDINES
}