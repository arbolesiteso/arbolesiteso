const TREE_ROUTE = {
  TITLE: "RUTA ÁRBOL NÚMERO ",
  NOT_FOUND: "404: Árbol no encontrado",
  ROUTE: "Ruta",
  NO_START_INSTRUCTIONS: "Elige tu punto de partida dando clic en el mapa.",
  INSTRUCTIONS: "El punto verde marca tu punto de partida, para continuar hacia tu arbol seleccionado comienza tu ruta.",
  START: "Comienza tu ruta",
}

module.exports = {
  TREE_ROUTE
}
