const AITESO_TITLE = "Árboles ITESO";
const DESCRIPTON = "Descripción:";
const START_SEARCH = "Comienza tu búsqueda";
const START_SEARCH_EX = "¡Comienza tu búsqueda!";
const WHAT_IS = "¿Qué es?";
const NUMBER = "NÚMERO";
const SPECIE = "ESPECIE";
const OK = "Ok";

module.exports = {
  AITESO_TITLE,
  DESCRIPTON,
  START_SEARCH,
  START_SEARCH_EX,
  WHAT_IS,
  NUMBER,
  SPECIE,
  OK,
}
