const HOME = {
  HOME_DESC : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris varius nibh in ante blandit, quis porttitor ex cursus. Suspendisse sollicitudin lacus vel justo luctus facilisis. Cras dapibus bibendum sagittis. Quisque eget sem quis justo maximus tristique. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
  NUMBER_TITLE : "BÚSQUEDA POR NÚMERO",
  NUMBER_DESC : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris varius nibh in ante blandit, quis porttitor ex cursus. Suspendisse sollicitudin lacus vel justo luctus facilisis. Cras dapibus bibendum sagittis. Quisque eget sem quis justo maximus tristique. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
  SPECIES_TITLE : "BÚSQUEDA POR ESPECIE",
  SPECIES_DESC : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris varius nibh in ante blandit, quis porttitor ex cursus. Suspendisse sollicitudin lacus vel justo luctus facilisis. Cras dapibus bibendum sagittis. Quisque eget sem quis justo maximus tristique. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.",
}

module.exports = {
    HOME
}