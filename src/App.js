import React from 'react'
import Navbar from './components/common/Navbar/Navbar'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import './App.css';
import LandingPage from './components/LandingPage/LandingPage';
import Home from './components/Home/Home';
import About from './components/About/About';
import Login from './components/Login/Login';
import SearchByNum from './components/SearchByNum/SearchByNum';
import SearchBySpe from './components/SearchBySpe/SearchBySpe';
import FiltroJardin from './components/FiltroJardin/FiltroJardin';
import TreeDetail from './components/TreeDetail/TreeDetail';
import TreeRoute from './components/TreeRoute/TreeRoute';

function App() {
  const CommonComponents = () => {
    return (
      <>
        <Navbar />
        <Switch>
          <Route path="/home" component={ Home } />
          <Route path="/about" component={ About } />
          <Route path="/login" component={ Login } />
          <Route path="/num-search" component={ SearchByNum } />
          <Route path="/spe-search" component={ SearchBySpe } />
          <Route path="/jardin-search/:specie_id" exact render={(props) => <FiltroJardin {...props} /> } />
          <Route path="/tree/:id" exact component={ TreeDetail } />
          <Route path="/tree/:id/route" component={ TreeRoute } />
        </Switch>
      </>
    )
  }

  return (
    <>
      <Router basename={process.env.REACT_APP_FILE_ROOT}>
        <Switch>
          <Route path="/" exact component={ LandingPage }/>
          <Route component={ CommonComponents } />
        </Switch>
      </Router>
    </>
  );
}

export default App;
